import { Pipe, PipeTransform } from '@angular/core';
import { string } from 'yup';

@Pipe({
  name: 'onSale',
})
export class OnSalePipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): string {
    if (value == true) {
      return 'Sale 50%';
    }
    return 'Not Sale';
  }
}
